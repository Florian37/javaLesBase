
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fbeauchef
 */
public class Exercice1Test 
{
    @Test
    public void testIsiInArrayOK()
    {
        int nbToFind = 85;
        int[] tab = {3, 8, 9, 85, -5};
        boolean res;
        res = Exercice1.isInArray(tab, nbToFind);
        assertTrue(res);
    }
    
    @Test
    public void testIsiInArrayKO()
    {
        int nbToFind = 89;
        int[] tab = {3, 8, 9, 85, -5};
        boolean res;
        res = Exercice1.isInArray(tab, nbToFind);
        assertFalse(res);
        
    }
        
}


 

