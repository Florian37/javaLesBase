
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fbeauchef
 */
public class Exercice2Test 
{
    @Test
    public void testIsiInArrayOK()
    {
        boolean res;
        int nb = 3;
        res = Exercice2.isInArray(nb);
        assertTrue(res);
    }
    
    @Test
    public void testIsiInArrayKO()
    {
        boolean res;
        int nb = 4;
        res = Exercice2.isInArray(nb);
        assertFalse(res);
    }
    
    @Test
    public void testIsiInArrayNeg()
    {
        boolean res;
        int nb = -7;
        res = Exercice2.isInArray(nb);
        assertFalse(res);
    }
}