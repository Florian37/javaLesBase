import java.util.Scanner;

public class Exercice3{
        char[] tab;
        String a; 
    public Exercice3(String s){
        a =s.replaceAll(" ","");
        tab = a.toCharArray();
    }
    public void isPalindrome (){
        int x = 0;
        int y =a.length()-1;
        while (y>x && tab[x]==tab[y]){
            x++;
            y--;
        }
        if (y<=x) 
        System.out.println("votre chaine est palindrome");
        else
        System.out.println("votre chaine n'est pas palindrome");
    }
    public static void main (String argv []){
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir le palindrome à tester :");
        String str = sc.nextLine();
        System.out.println("Vous avez saisi : " + str);
        Exercice3 m=new Exercice3(str);
        m.isPalindrome();
    }

}